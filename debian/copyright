Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: cppreference-doc
Upstream-Contact: Povilas Kanapickas <povilas@radix.lt>
Source: https://github.com/p12tic/emerald

Copyright: 2006 Quinn Storm <livinglatexkali@gmail.com>
           2006 David Reveman <davidr@novell.com>
License: GPL-2+

Files: engines/legacy.cc
Copyright: 2006 Quinn Storm <livinglatexkali@gmail.com>
License: GPL-2+

Files: engines/line.cc
Copyright: 2006 Quinn Storm <livinglatexkali@gmail.com>
           2007 Patrick Niklaus <patrick.niklaus@googlemail.com>
License: GPL-2+

Files: engines/oxygen.cc
Copyright: 2006 Quinn Storm <livinglatexkali@gmail.com>
           2006 Alain <big_al326@hotmail.com>
License: GPL-2+

Files: engines/pixmap.cc
Copyright: 2006 Quinn Storm <livinglatexkali@gmail.com>
           2006 Varun <varunratnakar@gmail.com>
License: GPL-2+

Files: engines/truglass.cc
Copyright: 2006 Quinn Storm <livinglatexkali@gmail.com>
           2006 Alain <big_al326@hotmail.com>
License: GPL-2+

Files: engines/vrunner.cc
Copyright: 2006 Quinn Storm <livinglatexkali@gmail.com>
           2006 Varun <varunratnakar@gmail.com>
License: GPL-2+

Files: engines/zootreeves.cc
Copyright: 2006 Quinn Storm <livinglatexkali@gmail.com>
           2006 Ben Reeves <john@reeves160.freeserve.co.uk>
License: GPL-2+

Files: libengine/*
       src/*
       themer/*
Copyright: 2006 Novell, Inc.
           2013-2014 Povilas Kanapickas <povilas@radix.lt>
License: GPL-2+

Files: po/*
Copyright: 2006-2010 Emerald Translators
License: GPL-2+

Files: m4/boost.m4
Copyright: 2007-2011  Benoit Sigoure <tsuna@lrde.epita.fr>
License: GPL-3+

Files: m4/*.m4
Copyright: 1996-2011  Free Software Foundation, Inc.
License: GPL-2+~libtool-exception

Files: install-sh
Copyright: 1994  X Consortium
License: X-Consortium-license
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
 TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of the X Consortium shall not
 be used in advertising or otherwise to promote the sale, use or other deal-
 ings in this Software without prior written authorization from the X Consor-
 tium.


Files: depcomp
       missing
       compile
Copyright: 1999-2013  Free Software Foundation, Inc
License: GPL-2+

Files: config.guess
       config-sub
Copyright: 1992-2013  Free Software Foundation, Inc
License: GPL-3+

Files: debian/*
Copyright: 2006  Quinn Storm <livinglatexkali@gmail.com> and
           2006  Shawn Starr <shawn.starr@rogers.com>
           2007-2008  Contributors
           2013-2014  Povilas Kanapickas <povilas@radix.lt>
License: GPL-3+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2, or (at your option) any
 later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
Comment:
 On Debian systems the 'GNU General Public License' version 2 is located
 in '/usr/share/common-licenses/GPL-2'.
 .
 You should have received a copy of the 'GNU General Public License'
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: GPL-2+~libtool-exception
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 .
 As a special exception to the GNU General Public License, if you
 distribute this file as part of a program that contains a
 configuration script generated by Autoconf, you may include it under
 the same distribution terms that you use for the rest of that program.
